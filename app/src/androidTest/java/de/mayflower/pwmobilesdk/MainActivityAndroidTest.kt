package de.mayflower.pwmobilesdk

import android.content.pm.ActivityInfo
import android.os.SystemClock
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import de.mayflower.pwmobilesdk.R
import io.mpos.transactionprovider.BaseTransactionProcessListener
import io.mpos.transactionprovider.TransactionProcessWithRegistrationListener
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import java.math.BigDecimal

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityAndroidTest {

    @get:Rule
    var mActivityTestRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun packageName_isCorrect() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        Assert.assertEquals("de.mayflower.pwmobilesdk", appContext.packageName)
    }

    @Test
    fun testAmountSetup_isCorrect() {
        val scenario :ActivityScenario<MainActivity> = launchActivity<MainActivity>()
        scenario.onActivity { activity : MainActivity ->
            Assert.assertEquals(BigDecimal(106), activity._testAmounts["DEFAULT"])
            Assert.assertEquals(23, activity._testAmounts.entries.size)
        }
    }

    @Test
    fun currencySpinner_defaultsToEUR() {
        onView(
            withId(R.id.input_currency),
        ).check(matches(withSpinnerText("EUR")))
    }

    @Test
    fun buttonTestTransaction_checkCaption() {
        onView(withId(R.id.button_test_transaction))
            .check(matches(withText("TEST TRANSACTION")))
    }

    @Test
    fun pushTransactionButton_performDefaultTransaction() {
        val transactionButton = onView(withId(R.id.button_test_transaction))
            .perform(ViewActions.click())

        // Thread.sleep(12500) is working just as well
        SystemClock.sleep(12500)

        val test = onView(withId(R.id.textview_log))

        val logOutput = onView(withId(R.id.textview_log))
            .check(matches(withText(
                "Starting new Transaction\n[Connecting to, card reader]\n[Registering, transaction]\n[106.00 EUR, Insert or swipe card], abortable: true\n[Processing payment..., ], abortable: true\n[Processing payment..., ], abortable: false\n[, Remove card], abortable: false\n[Processing payment..., ], abortable: false\n[Payment approved, ], abortable: false\n[Payment approved, Remove card], abortable: false\n[Payment approved, ], abortable: false\n[Payment approved, ], abortable: false\nTransaction completed\n\n"
            )))
    }

    @Test
    fun pushTransactionButton_noAmountEntered() {
        val amountInputView = onView(withId(R.id.input_amount))
            .perform(
                ViewActions.clearText(),
                ViewActions.closeSoftKeyboard()
            )

        val testTransactionButton = onView(withId(R.id.button_test_transaction))
            .perform(ViewActions.click())

        val logOutput = onView(withId(R.id.textview_log))
            .check(matches(withText("No valid amount provided\n")))
    }

    @Test
    fun pushTransactionButton_testProcessListener() {
        val processListenerMock : BaseTransactionProcessListener? = Mockito.spy(
            BaseTransactionProcessListener::class.java
        )

        val scenario : ActivityScenario<MainActivity> = launchActivity<MainActivity>()
        scenario.onActivity { activity : MainActivity ->
            activity._testProcessListener = processListenerMock
        }

        val testTransactionButton = onView(withId(R.id.button_test_transaction))
            .perform(ViewActions.click())

        SystemClock.sleep(12500)

        Mockito.verify(processListenerMock, Mockito.times(11))?.onStatusChanged(Mockito.any(), Mockito.any(), Mockito.any())
        Mockito.verify(processListenerMock, Mockito.times(1))?.onCompleted(Mockito.any(), Mockito.any(), Mockito.any())
        Mockito.verify(processListenerMock, Mockito.times(1))?.onRegistered(Mockito.any(), Mockito.any())
    }

    @Test
    fun startTransaction_processListener() {
        val scenario : ActivityScenario<MainActivity> = launchActivity<MainActivity>()
        val processListenerMock = Mockito.mock(TransactionProcessWithRegistrationListener::class.java)
        scenario.onActivity { activity : MainActivity ->
            activity._testProcessListener = processListenerMock

            val service = TransactionService()
            service.startTransaction(activity)
        }
        Mockito.verify(processListenerMock, Mockito.timeout(12500).times(11))
            .onStatusChanged(Mockito.any(), Mockito.any(), Mockito.any())
    }

    @Test
    fun amountMaxTwoDecimalPlaces() {
        onView(withId(R.id.input_amount))
            .perform(
                ViewActions.clearText(),
                ViewActions.typeText("77.88888888"),
                ViewActions.closeSoftKeyboard()
            )
            .check(matches(withText("77.88")))
    }

    @Test
    fun appTitle_firstFragment_isCorrect() {
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().context)
        onView(withText(R.string.action_mposSDK)).perform(ViewActions.click())
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(withText("Payworks mPOS SDK"))))
    }

    @Test
    fun appTitle_secondFragment_isCorrect() {
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().context)
        onView(withText(R.string.action_paybuttonSDK)).perform(ViewActions.click())
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(withText("Paybutton SDK"))))
    }

    @Test
    fun backButtonInSecondFragment_notPresent() {
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().context)
        onView(withText(R.string.action_paybuttonSDK)).perform(ViewActions.click())
        onView(Matchers.allOf(withContentDescription("Navigate up"))).check(doesNotExist())
    }

    @Test
    fun allUiElementsVisible_portrait() {
        val scenario :ActivityScenario<MainActivity> = launchActivity<MainActivity>()
        scenario.onActivity { activity : MainActivity ->
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }

        onView(withId(R.id.textview_log_scroll_view)).check(matches(isDisplayed()))
        onView(withId(R.id.input_amount)).check(matches(isDisplayed()))
        onView(withId(R.id.input_currency)).check(matches(isDisplayed()))
        onView(withId(R.id.input_test_amount)).check(matches(isDisplayed()))
        onView(withId(R.id.button_test_transaction)).check(matches(isDisplayed()))
    }

    @Test
    fun allUiElementsVisible_landscape() {
        val scenario :ActivityScenario<MainActivity> = launchActivity<MainActivity>()
        scenario.onActivity { activity : MainActivity ->
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }

        onView(withId(R.id.textview_log_scroll_view)).check(matches(isDisplayed()))
        onView(withId(R.id.input_amount)).check(matches(isDisplayed()))
        onView(withId(R.id.input_currency)).check(matches(isDisplayed()))
        onView(withId(R.id.input_test_amount)).check(matches(isDisplayed()))
        onView(withId(R.id.button_test_transaction)).check(matches(isDisplayed()))
    }
}
