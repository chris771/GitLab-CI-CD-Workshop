package de.mayflower.pwmobilesdk

import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.view.View
import io.mpos.transactionprovider.TransactionProcess
import io.mpos.transactionprovider.TransactionProvider
import io.mpos.transactions.Currency
import io.mpos.transactions.parameters.TransactionParameters
import io.mpos.ui.shared.MposUi
import io.mpos.ui.shared.model.MposUiAppearance
import io.mpos.ui.shared.model.MposUiConfiguration
import org.junit.Test
import org.mockito.Mockito

class TransactionServiceTest {
    @Test
    fun startTransaction_emptyAmount() {
        val mainActivityMock : MainActivity = Mockito.mock(MainActivity::class.java)
        Mockito.`when`(mainActivityMock.getInputAmount()).thenReturn("")
        val service = TransactionService()
        service.startTransaction(mainActivityMock)

        Mockito.verify(mainActivityMock, Mockito.times(1)).getInputAmount()
        Mockito.verify(mainActivityMock, Mockito.times(1))?.logToConsole("No valid amount provided", Color.RED)
    }

    @Test
    fun startTransaction_success() {
        val mainActivityMock: MainActivity = Mockito.mock(MainActivity::class.java)
        val transactionProviderMock = Mockito.mock(TransactionProvider::class.java)

        Mockito.`when`(mainActivityMock.getInputAmount()).thenReturn("5.00")
        Mockito.`when`(mainActivityMock.getInputCurrency()).thenReturn(Currency.EUR)
        Mockito.`when`(mainActivityMock.getTransactionProvider()).thenReturn(transactionProviderMock)

        val service = TransactionService()
        service.startTransaction(mainActivityMock)

        Mockito.verify(mainActivityMock, Mockito.times(1)).getInputAmount()
        Mockito.verify(mainActivityMock, Mockito.times(1)).getInputCurrency()
        Mockito.verify(mainActivityMock, Mockito.times(1)).logToConsole("Starting new Transaction")
        Mockito.verify(mainActivityMock, Mockito.times(1)).setButtonAbortTransactionVisibilityAndEnabled(View.VISIBLE, false)
        Mockito.verify(mainActivityMock, Mockito.times(1)).setButtonStartTransactionVisibility(View.GONE)
        Mockito.verify(mainActivityMock, Mockito.times(1)).getTransactionProvider()
    }

    @Test
    fun startPaybuttonTransaction_success() {
        val mainActivityMock: MainActivity = Mockito.mock(MainActivity::class.java)
        val resourcesMock: Resources = Mockito.mock(Resources::class.java)
        Mockito.`when`(mainActivityMock.resources).thenReturn(resourcesMock)
        val mposUiMock: MposUi = Mockito.mock(MposUi::class.java)
        val mposUiConfigurationMock: MposUiConfiguration = Mockito.mock(MposUiConfiguration::class.java)
        Mockito.`when`(mposUiMock.configuration).thenReturn(mposUiConfigurationMock)
        val mposUiAppearanceMock: MposUiAppearance = Mockito.mock(MposUiAppearance::class.java)
        Mockito.`when`(mposUiConfigurationMock.appearance).thenReturn(mposUiAppearanceMock)
        val intentMock = Mockito.mock(Intent::class.java)
        Mockito.`when`(mposUiMock.createTransactionIntent(Mockito.any(TransactionParameters::class.java))).thenReturn(intentMock)

        Mockito.mockStatic(MposUi::class.java).use { theMock ->
            theMock.`when`<Any> { MposUi.initialize(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()) }
                .thenReturn(mposUiMock)

            val service = TransactionService()
            service.startPaybuttonTransaction(mainActivityMock)

            Mockito.verify(mposUiMock, Mockito.times(1)).createTransactionIntent(Mockito.any(TransactionParameters::class.java))
            Mockito.verify(mainActivityMock, Mockito.times(1)).startActivityForResult(Mockito.any(), Mockito.eq(MposUi.REQUEST_CODE_PAYMENT))
            Mockito.verify(mposUiConfigurationMock, Mockito.times(1)).setSummaryFeatures(Mockito.any())
        }
    }

    @Test
    fun abortTransaction_success() {
        val mainActivityMock: MainActivity = Mockito.mock(MainActivity::class.java)
        val transactionProcessMock = Mockito.mock(TransactionProcess::class.java)

        Mockito.`when`(mainActivityMock._process).thenReturn(transactionProcessMock)

        val service = TransactionService()
        service.abortTransaction(mainActivityMock)

        Mockito.verify(transactionProcessMock, Mockito.times(1)).requestAbort()
    }
}