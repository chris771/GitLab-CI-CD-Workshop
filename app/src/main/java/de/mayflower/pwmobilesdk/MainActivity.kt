package de.mayflower.pwmobilesdk

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import de.mayflower.pwmobilesdk.databinding.ActivityMainBinding
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.*
import io.mpos.provider.ProviderMode
import io.mpos.transactions.Currency
import java.math.BigDecimal
import io.mpos.Mpos
import io.mpos.mock.DefaultMockConfiguration
import kotlin.collections.HashMap
import android.text.Spanned
import android.text.InputFilter
import io.mpos.ui.shared.MposUi
import java.lang.StringBuilder
import android.widget.Toast
import android.content.Intent
import de.mayflower.pwmobilesdk.R
import io.mpos.transactionprovider.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    var _process: TransactionProcess? = null

    private var _scrollViewTextLog: ScrollView? = null
    private var _textViewLog: TextView? = null
    var _buttonStartTransaction: Button? = null
    var _buttonAbortTransaction: Button? = null
    private var _buttonClearLog: Button? = null
    var _inputAmount: EditText? = null
    var _inputCurrency: Spinner? = null
    private var _inputTestAmount: Spinner? = null

    private var _buttonPaybutton: Button? = null

    private var _decimalDigitFilter: InputFilter = object : InputFilter {
        override fun filter(
            source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int
        ): CharSequence? {
            val builder = StringBuilder(dest)
            builder.replace(dstart, dend, source.subSequence(start, end).toString())

            // flawless if RegEx matches
            if (builder.toString().matches(Regex("\\d*(\\.\\d{0,2})?\$"))) {
                return null
            }

            // return cropped but valid string
            return if (source.isEmpty()) "" else dest.subSequence(dstart, dend)
        }
    }

    var _transactionAbortInitialized: Boolean = false;

    var _testAmounts = HashMap<String, BigDecimal>()
    var _testProcessListener :TransactionProcessWithRegistrationListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        this._scrollViewTextLog      = findViewById(R.id.textview_log_scroll_view)
        this._textViewLog            = findViewById(R.id.textview_log)
        this._buttonStartTransaction = findViewById(R.id.button_test_transaction)
        this._buttonAbortTransaction = findViewById(R.id.button_test_abort)
        this._buttonClearLog         = findViewById(R.id.button_clear_log)
        this._inputAmount            = findViewById(R.id.input_amount)
        this._inputCurrency          = findViewById(R.id.input_currency)
        this._inputTestAmount        = findViewById(R.id.input_test_amount)

        //disable abort transaction button initially
        this._buttonAbortTransaction?.isEnabled = false;

        //setup inpput validation
        Log.d("filters", this._inputAmount?.filters.toString())
        this._inputAmount?.setFilters(arrayOf(this._decimalDigitFilter))

        //setup currency spinner
        this._inputCurrency?.adapter = ArrayAdapter<Currency>(this, android.R.layout.simple_list_item_1, Currency.values())
        this._inputCurrency?.setSelection(Currency.EUR.ordinal)

        //setup test amount spinner
        setTestAmounts()

        val sortedAmountKeys = _testAmounts.keys.toMutableList().sorted()
        this._inputTestAmount?.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sortedAmountKeys)

        val defaultIndex = sortedAmountKeys.indexOf("DEFAULT")
        this._inputTestAmount?.setSelection(defaultIndex)

        this._inputTestAmount?.onItemSelectedListener = this

        //assign button callback listeners
        this._buttonStartTransaction?.setOnClickListener {
            startTransaction()
        }

        this._buttonAbortTransaction?.setOnClickListener {
            abortTransaction()
        }

        this._buttonClearLog?.setOnClickListener {
            clearLog()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Log.d( "payworks", "onOptionsItemSelected" )

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        if (item.itemId == R.id.action_paybutton_sdk && navController.currentDestination?.id == R.id.FirstFragment) {
            navController.navigate(R.id.action_FirstFragment_to_SecondFragment)
        } else if (item.itemId == R.id.action_mposSDK && navController.currentDestination?.id == R.id.SecondFragment) {
            navController.navigate(R.id.action_SecondFragment_to_FirstFragment)
        }

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_mposSDK -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MposUi.REQUEST_CODE_PAYMENT) {
            if (resultCode == MposUi.RESULT_CODE_APPROVED) {
                Toast.makeText(this, "Transaction approved", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(
                    this, "Transaction was declined, aborted, or failed",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun clearLog() {
        _textViewLog?.text = ""
    }

    fun setTestAmounts() {
        val mockConf = DefaultMockConfiguration()
        val fields = mockConf.javaClass.declaredFields
        var amounts = HashMap<String, BigDecimal>()

        for (field in fields) {
            if (field.name.contains("AMOUNT_", ignoreCase = false)) {
                val amount = field.get(mockConf) as BigDecimal
                amounts[field.name.removePrefix("AMOUNT_")] = amount
            }
        }

        _testAmounts = amounts
    }

    fun startPaybuttonTransaction() {
        val transactionService = TransactionService()
        transactionService.startPaybuttonTransaction(this)
    }

    fun startTransaction() {
        val transactionService = TransactionService()
        transactionService.startTransaction(this)
    }

    fun logToConsole(lineToLog: String = "", color: Int = Color.DKGRAY) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val hexColor :String = String.format("#%06X", 0xffffff and color)
            _textViewLog?.append(
                Html.fromHtml("<span style=\"color: $hexColor\">$lineToLog</span><br>",Html.FROM_HTML_MODE_COMPACT)
            )
        } else {
            _textViewLog?.append(lineToLog + "\n")
        }

        scrollTextViewLogToBottom()
    }

    private fun scrollTextViewLogToBottom() {
        _scrollViewTextLog?.fullScroll(View.FOCUS_DOWN);
    }

    override fun onBackPressed() {
        Toast.makeText(
            this,
            "The back button is disabled during a transaction. Please use the 'abort' button to cancel the transaction.",
            Toast.LENGTH_LONG
        ).show()
    }

    fun abortTransaction() {
        val transactionService = TransactionService()
        transactionService.abortTransaction(this)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
        val selected = parent?.getItemAtPosition(pos)
        val amountString = _testAmounts[selected].toString()
        if (amountString != null) {
            _inputAmount?.setText(amountString)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        _inputAmount?.setText("")
    }

    fun getInputAmount() :String {
        return _inputAmount?.text.toString()
    }

    fun getInputCurrency() :Currency {
        return _inputCurrency?.selectedItem as Currency
    }

    fun setButtonStartTransactionVisibility(visibility:Int) {
        _buttonStartTransaction?.visibility = visibility
    }

    fun setButtonAbortTransactionVisibilityAndEnabled(visibility:Int, isEnabled:Boolean) {
        _buttonAbortTransaction?.visibility = visibility
        _buttonAbortTransaction?.isEnabled = isEnabled
    }

    fun getTransactionProvider(): TransactionProvider {
        return Mpos.createTransactionProvider(
            this,
            ProviderMode.MOCK,
            "cbf4d153-e78a-4937-8ece-6b1ec948a2f9",
            "ZCMNdotEb3dLkRWabOxUsqe20hDq31ml"
        )
    }
     fun getProcessListener(): TransactionProcessWithRegistrationListener? {
         return _testProcessListener
     }
}
