package de.mayflower.pwmobilesdk

import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import android.view.View
import de.mayflower.pwmobilesdk.R
import io.mpos.accessories.AccessoryFamily
import io.mpos.accessories.parameters.AccessoryParameters
import io.mpos.android.shared.BitmapHelper
import io.mpos.paymentdetails.ApplicationInformation
import io.mpos.paymentdetails.DccInformation
import io.mpos.provider.ProviderMode
import io.mpos.transactionprovider.TransactionProcess
import io.mpos.transactionprovider.TransactionProcessDetails
import io.mpos.transactionprovider.TransactionProcessDetailsState
import io.mpos.transactionprovider.TransactionProcessWithRegistrationListener
import io.mpos.transactions.Currency
import io.mpos.transactions.Transaction
import io.mpos.transactions.parameters.TransactionParameters
import io.mpos.transactions.receipts.Receipt
import io.mpos.ui.shared.MposUi
import io.mpos.ui.shared.model.MposUiConfiguration
import java.math.BigDecimal
import java.util.*

class TransactionService {

    fun startTransaction(activity: MainActivity) {
        val amount: String = activity.getInputAmount()

        if (amount.isEmpty()) {
            activity.logToConsole("No valid amount provided", Color.RED)
            return
        }
        val decimalAmount: BigDecimal = amount.toBigDecimal()

        val currency = activity.getInputCurrency()

        // log new transaction start
        activity.logToConsole("Starting new Transaction")

        // disable button 'start transaction'
        activity.setButtonStartTransactionVisibility(View.GONE)
        activity.setButtonAbortTransactionVisibilityAndEnabled(View.VISIBLE, false)

        // For starting transaction in mocked mode use fallowing provider:
        val transactionProvider = activity.getTransactionProvider()

        /* When using the Bluetooth Miura, use the following parameters: */
        val accessoryParameters = AccessoryParameters.Builder(AccessoryFamily.MOCK)
            .mocked()
            .build()
/*
        // when using Verifone readers via WiFi or Ethernet, use the following parameters:
        AccessoryParameters accessoryParameters = new AccessoryParameters.Builder(AccessoryFamily.VERIFONE_VIPA)
            .tcp("192.168.254.123", 16107)
            .build();
*/
        val transactionParameters = TransactionParameters.Builder()
            .charge(decimalAmount, currency)
            .subject("Bouquet of Flowers")
            .customIdentifier("yourReferenceForTheTransaction")
            .build()
        val paymentProcess = transactionProvider.startTransaction(
            transactionParameters,
            accessoryParameters,
            (
                    activity.getProcessListener() ?: object : TransactionProcessWithRegistrationListener {
                        override fun onRegistered(
                            process: TransactionProcess?,
                            transaction: Transaction
                        ) {
                            Log.d(
                                "mpos",
                                "transaction identifier is: " + transaction.getIdentifier()
                                    .toString() + ". Store it in your backend so that you can always query its status."
                            )
                        }

                        override fun onStatusChanged(
                            process: TransactionProcess?,
                            transaction: Transaction?,
                            processDetails: TransactionProcessDetails
                        ) {
                            var lineToLog = Arrays.toString(processDetails.information)

                            Log.d("mpos", "status changed: " + lineToLog)

                            activity._process = process

                            if (transaction != null) {
                                if (transaction.canBeAborted() && !activity._transactionAbortInitialized) {
                                    activity._buttonAbortTransaction?.isEnabled = true
                                    lineToLog += ", abortable: " + transaction.canBeAborted()
                                } else {
                                    activity._buttonAbortTransaction?.isEnabled = false
                                    lineToLog += ", abortable: " + transaction.canBeAborted()
                                }
                            }

                            activity.logToConsole(lineToLog, Color.BLUE)
                        }

                        override fun onCustomerSignatureRequired(
                            process: TransactionProcess,
                            transaction: Transaction?
                        ) {
                            // in a live app, this image comes from your signature screen
                            val conf = Bitmap.Config.ARGB_8888
                            val bm = Bitmap.createBitmap(1, 1, conf)
                            val signature = BitmapHelper.byteArrayFromBitmap(bm)
                            process.continueWithCustomerSignature(signature, true)
                        }

                        override fun onCustomerVerificationRequired(
                            process: TransactionProcess,
                            transaction: Transaction?
                        ) {
                            // always return false here
                            process.continueWithCustomerIdentityVerified(false)
                        }

                        override fun onApplicationSelectionRequired(
                            process: TransactionProcess,
                            transaction: Transaction?,
                            applicationInformation: List<ApplicationInformation?>
                        ) {
                            // This happens only for readers that don't support application selection on their screen
                            process.continueWithSelectedApplication(applicationInformation[0])
                        }

                        override fun onDccSelectionRequired(
                            transactionProcess: TransactionProcess,
                            transaction: Transaction?,
                            dccInformation: DccInformation?
                        ) {

                            // This comes up if the DCC selection cannot be done on the terminal itself
                            transactionProcess.continueDccSelectionWithOriginalAmount()
                        }

                        override fun onCompleted(
                            process: TransactionProcess?,
                            transaction: Transaction,
                            processDetails: TransactionProcessDetails
                        ) {
                            Log.d("mpos", "completed")

                            activity._buttonStartTransaction?.visibility = View.VISIBLE
                            activity._buttonAbortTransaction?.visibility = View.GONE

                            activity.logToConsole("Transaction completed")
                            activity.logToConsole()

                            if (processDetails.state == TransactionProcessDetailsState.ABORTED) {
                                activity._transactionAbortInitialized = false;
                            }

                            if (processDetails.state == TransactionProcessDetailsState.APPROVED) {
                                // print the merchant receipt
                                val merchantReceipt: Receipt = transaction.getMerchantReceipt()

                                // print a signature line if required
                                if (merchantReceipt.isSignatureLineRequired()) {
                                    println("")
                                    println("")
                                    println("")
                                    println("------ PLEASE SIGN HERE ------")
                                }

                                // ask the merchant, whether the shopper wants to have a receipt
                                val customerReceipt: Receipt = transaction.getCustomerReceipt()

                                // and close the checkout UI
                            } else {
                                // Allow your merchant to try another transaction
                            }
                        }
                    })
        )
    }

    fun abortTransaction(activity: MainActivity) {
        if (activity._transactionAbortInitialized) {
            Log.i( "mpos", "transaction abort already initialized" )
            return
        }
        activity._transactionAbortInitialized = true;
        activity._process?.requestAbort()
    }

    fun startPaybuttonTransaction(activity: MainActivity) {
        var ui = MposUi.initialize(activity, ProviderMode.MOCK, "dead", "beef")
        ui.configuration.summaryFeatures = EnumSet.of(MposUiConfiguration.SummaryFeature.SEND_RECEIPT_VIA_EMAIL)
        ui.configuration.appearance.colorPrimary = activity.resources.getColor(R.color.mayflower_orange)

        var accessoryParameters = AccessoryParameters.Builder(AccessoryFamily.MOCK).mocked().build()
        ui.configuration.terminalParameters = accessoryParameters

        var transactionParameters = TransactionParameters.Builder()
            .charge(BigDecimal("5.00"), Currency.EUR)
            .subject("Test")
            .customIdentifier("reference")
            .build()

        val intent = ui.createTransactionIntent(transactionParameters)
        activity.startActivityForResult(intent, MposUi.REQUEST_CODE_PAYMENT)
    }
}
