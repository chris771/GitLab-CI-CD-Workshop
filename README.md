# Payworks Mobile Payment SDK for Android
This Android App is a test implementation of the [Payworks Mobile Payment SDK for Android](https://payworks.mpymnt.com/cp_int_pos_custom_overview/cp_int_pos_custom_installation.html)

# Manual
The following manual has been used in order to implement the Payworks mPOS-SDK and the transaction testcode:
https://payworks.mpymnt.com/cp_int_pos_custom_overview/cp_int_pos_custom_installation.html

---

# Primary Gradle Tasks
Find all primary **Gradle Tasks** and their according commands being used inside this project here:

### Show All Tasks
Shows all applicable Gradle Tasks for this project.
```
./gradlew tasks
```

### Clean
Ditch the entire `app/build` folder.
```
./gradlew :app:clean --info
```

### Assemble Debug
Perform a clean local build.
```
./gradlew :app:assembleDebug --info
```

### Clean Build
Add multiple targets for a clean build:
```
./gradlew :app:clean :app:assembleDebug --info
```

### Create APK packages
Assembles the bundles for all variants (**debug** and **release**).
Application packages (new **AAB** file format) are stored in `build/outputs/bundle/`.
```
./gradlew :app:bundle --info
```

### Run Unit Tests
Run all **Unit Tests** in **debug** and **release** mode. `--info` delivers log output to the console.
Yields an JUnit XML test report in `build/test-results/testDebugUnitTest/TEST-de.mayflower.pwmobilesdk.ExampleUnitTest.xml`.
```
./gradlew :app:test --info
```

### Run Android Instrumented Tests
Install and run all **Android Instrumented Tests** in **debug** and **release** mode on connected devices.
`--info` delivers log output to the console.
Be sure to start the **Android Emulator** before running this command:
```
./gradlew :app:connectedAndroidTest --info
```

### Run Android Linter
Invoke the (internal) Android Lint Analysis and report potential code problems.
Linter report is generated inside `build/reports/`.
```
./gradlew :app:lint --info
```

### Run Static Code Inspection
Invoke the Kotlin Coding Style Linter **detekt**. Generates an XML report
in **Checkstyle** format at `build/reports/detekt/detekt.xml`.
Any coding style violation will cause this Gradle task to fail.
```
./gradlew :app:detekt --info
```

### Generate JaCoCo Code Coverage
Generate Code Coverage. The task `:app:test` must be run in forehand of this task!
Coverage reports are generated in `build/reports/jacoco`.
```
./gradlew :app:jacocoTestReport --info
```

